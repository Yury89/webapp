
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8" %>

<div class="row">
<%@include file="../WEB-INF/jsps/header.jspf" %>
<div class="col s12 m4 l3">
        <ul class="collection with-header">
            <li class="collection-header"><h4>Жанры</h4></li>
            <jsp:useBean id="genres" class="beans.GenreList"/>
            <a href="/MyApp/jsp/main.jsp" class="collection-item">Все жанры</a>
            <c:forEach var="genre" items="${genres.getGenres()}">
                <a href="?name=${genre.id}" class="collection-item">${genre.name}</a>
            </c:forEach>
        </ul>
</ul>
    </div>

    <div class="col s12 m8 l9">
<div class="row">
    <jsp:useBean id="bookList1" class="beans.BookList" scope="session"/>

    <c:if test="${pageContext.request.getParameter('name') == null}">

        <c:set var="bookList" value="${bookList1.getAllBooks()}"/>

    </c:if>
    <c:if test="${pageContext.request.getParameter('name') != null}">

        <c:set var="bookList" value="${bookList1.getBookByGenres(pageContext.request.getParameter('name'))}"/>
    </c:if>
    <c:if test="${pageContext.request.getParameter('search') != null && pageContext.request.getParameter('search') != ''}">
        <c:set var="bookList" value="${bookList1.getBooksBySearch(pageContext.request.getParameter('search'),pageContext.request.getParameter('type'))}"/>
    </c:if>

    <c:forEach var="books" items="${bookList}">

   <div class="card medium col s12 m12  l4">
    <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="/MyApp/photo?id=${books.id}" >
    </div>
    <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">${books.name}<i
                class="material-icons right">more_vert</i></span>
        <p><a href="#">Характеристика</a></p>
    </div>
    <div class="card-reveal">
        <span class="card-title grey-text text-darken-4">Характеристика<i class="material-icons right">close</i></span>
        <jsp:useBean id="descr" class="beans.BookDiscribeList" scope="session"/>
       <c:set var="descrV" value="${descr.getdataFromDB(books.id)}"/>
        <table>
            <tbody>
            <tr>
                <td>Имя книги</td>
                <td>${descrV.nameBook}</td>

            </tr>
            <tr>
                <td>Кол-во страниц</td>
                <td>${descrV.pageCount}</td>

            </tr>

            <tr>
                <td>Жанр</td>
                <td>${descrV.genreName}</td>

            </tr>
            <tr>
                <td>Автор</td>
                <td>${descrV.authorName}</td>

            </tr>
            <tr>
                <td>Дата публикации</td>
                <td>${descrV.publishYear}</td>

            </tr>
            <tr>
                <td>Издательство</td>
                <td>${descrV.publisherName}</td>

            </tr>
            </tbody>
        </table>

    </div>
    </div>
</c:forEach>

        </div>
    </div>
</div>

<%@include file="../WEB-INF/jsps/footer.jspf" %>


package beans;

/**
 * Created by Yuriy on 2/10/2016.
 */
import java.util.Date;
public class BookDescribe {
    private String nameBook;
    private int pageCount;
    private String isbn;
    private String genreName;
    private String authorName;
    private Date publishYear;
    private String publisherName;

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Date publishYear) {
        this.publishYear = publishYear;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public BookDescribe(String nameBook, int pageCount, String isbn, String genreName, String authorName, Date publishYear, String publisherName) {

        this.nameBook = nameBook;
        this.pageCount = pageCount;
        this.isbn = isbn;
        this.genreName = genreName;
        this.authorName = authorName;
        this.publishYear = publishYear;
        this.publisherName = publisherName;
    }

    public BookDescribe() {

    }
}

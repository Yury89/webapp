package beans;

import database.DatabaseCon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yuriy on 2/9/2016.
 */
public class GenreList {
    private List<Genre> genres = new ArrayList<>();

    {

        try(Connection con = new DatabaseCon().getConnection();
            Statement statement = con.createStatement()
        ) {
            ResultSet res = statement.executeQuery("select id,name FROM genre");
            while (res.next())
                genres.add(new Genre(res.getLong("id"),res.getString("name")));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<Genre> getGenres() {
        return genres;
    }
}

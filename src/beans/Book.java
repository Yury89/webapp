package beans;

/**
 * Created by Yuriy on 2/9/2016.
 */
import java.util.Date;
public class Book {
    private long id;
    private long genreId;
    private long authorId;
    private String name;
    private int pageCount;
    private String isBn;
    private Date publisherYear;
    private long publisherId;

    public Book(long id, long genreId, long authorId, String name, int pageCount, String isBn, Date publisherYear, long publisherId) {
        this.id = id;
        this.genreId = genreId;
        this.authorId = authorId;
        this.name = name;
        this.pageCount = pageCount;
        this.isBn = isBn;
        this.publisherYear = publisherYear;
        this.publisherId = publisherId;
    }

    public Book() {

    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long genreId) {
        this.genreId = genreId;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getIsBn() {
        return isBn;
    }

    public void setIsBn(String isBn) {
        this.isBn = isBn;
    }

    public Date getPublisherYear() {
        return publisherYear;
    }

    public void setPublisherYear(Date publisherYear) {
        this.publisherYear = publisherYear;
    }

    public long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(long publisherId) {
        this.publisherId = publisherId;
    }
}

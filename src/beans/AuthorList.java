package beans;

import database.DatabaseCon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yuriy on 2/9/2016.
 */
public class AuthorList {

    private List<Author> authors = new ArrayList<>();

    public List<Author> getAuthors() {
        return authors;
    }

    {
        Connection con = new DatabaseCon().getConnection();
        try (Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT id,fio,birthday from author ORDER bY fio")) {

            while (resultSet.next())
                authors.add(new Author(resultSet.getLong("id"), resultSet.getString("fio"), resultSet.getDate("birthday")));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {

                if (con != null)
                    con.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public AuthorList() {
    }


}

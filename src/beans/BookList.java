package beans;

import database.DatabaseCon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class BookList {


    public ArrayList<Book> getAllBooks()

    {
        ArrayList<Book> books = new ArrayList<>();
        try (Connection connection = new DatabaseCon().getConnection()) {

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select id,genre_id,author_id, name, page_count,isbn,publish_year,publisher_id FROM book");
            while (rs.next())
                books.add(new Book(rs.getLong("id"), rs.getLong("genre_id"), rs.getLong("author_id"), rs.getString("name"),
                        rs.getInt("page_count"),
                        rs.getString("isbn"),
                        rs.getDate("publish_year"),
                        rs.getLong("publisher_id")));
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return books;
    }

    public ArrayList<Book> getBookByGenres(int genresId) {

        ArrayList<Book> books = new ArrayList<>();
        try (Connection connection = new DatabaseCon().getConnection()) {

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select *" +
                    " FROM book" +
                    " WHERE genre_id=" + genresId);
            while (rs.next())
                books.add(new Book(rs.getLong("id"), rs.getLong("genre_id"), rs.getLong("author_id"), rs.getString("name"),
                        rs.getInt("page_count"),
                        rs.getString("isbn"),
                        rs.getDate("publish_year"),
                        rs.getLong("publisher_id")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return books;
    }


    public BookList() {
    }


    public ArrayList<Book> getBooksBySearch(String search, String type) {

        System.out.println(search + " " + type);
        ArrayList<Book> books = new ArrayList<>();
        String sql = "";
        if(type.equals("author"))
             sql = " select *\n" +
                     " FROM book\n" +
                     " join author on book.author_id = author.id\n" +
                     " where author.fio like '%" + search + "%'";
        if(type.equals("book"))
            sql = "select *" +
                    "from book where name like  '%" + search + "%'";
        try (Connection connection = new DatabaseCon().getConnection()) {

            System.out.println(sql);
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next())
                books.add(new Book(rs.getLong("id"), rs.getLong("genre_id"), rs.getLong("author_id"), rs.getString("name"),
                        rs.getInt("page_count"),
                        rs.getString("isbn"),
                        rs.getDate("publish_year"),
                        rs.getLong("publisher_id")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;

    }
}

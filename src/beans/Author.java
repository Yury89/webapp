package beans;

import java.util.Date;

public class Author {

    private long id;
    private String fio;
    private Date birthday;

    public Author(long id, String fio, Date birthday) {
        this.id = id;
        this.fio = fio;
        this.birthday = birthday;
    }

    public Author() {

    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


}

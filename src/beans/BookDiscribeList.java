package beans;

import database.DatabaseCon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class BookDiscribeList {
    private BookDescribe describe = new BookDescribe();

    public void setDescribe(BookDescribe describe) {
        this.describe = describe;
    }

    public BookDescribe getDescribe() {

        return describe;
    }

    public BookDiscribeList() {

    }

    public BookDescribe getdataFromDB(int id) {
        ArrayList<BookDescribe> list = new ArrayList<>();
        try (Connection con = new DatabaseCon().getConnection(); Statement st = con.createStatement()) {
            ResultSet rs = st.executeQuery(
                   "SELECT book.name, book.page_count,book.isbn, genre.name AS gName, author.fio, book.publish_year ,publisher.name as pName\n" +
                           "FROM book\n" +
                           "JOIN author ON book.author_id = author.id\n" +
                           "JOIN genre ON book.genre_id = genre.id\n" +
                           "JOIN publisher ON book.publisher_id = publisher.id\n" +
                           "WHERE book.id  = " + id
                            );
            rs.next();
            return new BookDescribe(rs.getString("name"),
                    rs.getInt("page_count"),
                    rs.getString("isbn"),
                    rs.getString("gName"),
                    rs.getString("fio"),
                    rs.getDate("publish_year"),
                    rs.getString("pName"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package servlets;

import database.DatabaseCon;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Yuriy on 2/12/2016.
 */
public class Servlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Connection connection = new DatabaseCon().getConnection(); Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery("SELECT image FROM book WHERE id=" + req.getParameter("id")) ) {
            resp.setContentType("image/jpeg");
            rs.next();
            OutputStream out = resp.getOutputStream();
            out.write(rs.getBytes("image"));
            out.flush();
            out.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

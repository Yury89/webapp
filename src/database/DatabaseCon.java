package database;


import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Yuriy on 2/8/2016.
 */
public class DatabaseCon {
    InitialContext initialContext;
    DataSource dataSource;

    public Connection getConnection() {
        try {
            initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("jdbc/library");
            return dataSource.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                initialContext.close();

            } catch (NamingException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
